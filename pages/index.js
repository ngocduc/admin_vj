import { useState, useCallback } from 'react';

import { Layout, Menu, Row, Col, Button, Input, Spin, Radio, Modal } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
  UploadOutlined,
  SearchOutlined,
  PlusOutlined,
  MinusOutlined,
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined
} from '@ant-design/icons';
import { get } from 'lodash';
import styled from 'styled-components';

// import SortableTree from 'react-sortable-tree';
// import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app

// import '../public/styles.css';

import { useMenuState } from '../handler/globalHook';

const baseApi = 'https://apps.vietjack.com:8080/';

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const App = () => {
  const [menuState, actionMenu] = useMenuState();
  const [dataModal, setDataModal] = useState(false);

  const handleOk = e => {
    actionMenu.resetStateMenu()
  };

  const handleCancel = e => {
    console.log('-------', e)
    actionMenu.resetStateMenu();

  };

  const [treeData, setDataTree] = useState([
    { title: 'Chicken', children: [{ title: 'Egg' }] },
    { title: 'Fish', children: [{ title: 'fingerline'}] }
  ])

  return (
    <Layout>
      <Header
        theme='light'
        style={{
          position: 'fixed',
          zIndex: 1, width: '100%',
          background: '#ddd',
          boxShadow: '0 2px 8px #f0f1f2',
          padding: 0,
        }}>
        <Row>
          {/* icon */}
          <Col style={{ background: '#ddd' }} xs={{ span: 20 }} sm={{ span: 4 }} md={{ span: 4 }}>
            {/* Home */}
            {' '}
          </Col>
          {/* search */}
          <Col style={{ background: '#ddd' }} xs={{ span: 0 }} sm={{ span: 8 }} md={{ span: 6 }}>
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
          </Col>
          {/* menu */}
          <Col style={{ background: '#ddd' }} xs={{ span: 0 }} sm={{ span: 10 }} md={{ span: 12 }}>
            {' '}
            {/* Col */}
          </Col>
          {/* right */}
          <Col style={{ background: '#ddd' }} xs={{ span: 4 }} sm={{ span: 2 }} md={{ span: 2 }}>
            {' '}
            {/* icon */}
          </Col>
        </Row>
      </Header>
      <Content
        className="site-layout-background"
        style={{
          margin: '70px 16px 20px 16px',
          minHeight: 280,
          background: '#f0f2f5'
        }}
      >
        <div>
          {/* menu */}
          <MenuApp />

          {/* <div style={{ height: 400 }}>
            <SortableTree
              treeData={treeData}
              onChange={treeData => setDataTree(treeData)}
            />
          </div> */}
          <Modal
            title={`${get(menuState, 'type', 'Modal')}`}
            visible={!!get(menuState, 'type', '')}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Modal>
        </div>
        {/* <AppItem /> */}
      </Content>
    </Layout>

  );

}

export default App;

const colors = {
  1: '#F18759',
  2: '#A2C1FD',
  3: '#9CCD85',
  'isExpand': '#fff'
}

const colorsHV = {
  1: '#EC6027',
  2: '#87AEFD',
  3: '#61B13D',
  'isExpand': '#fff'
}

// handleColor = (level) => {
//   return 
// }

const Title = (props) => {
  const actionMenu = useMenuState()[1];
  const {
    text = "",
    handleClick = () => { },
    isLoading = false,
    level = 1,
    isExpand = false,
    isLast,
    type = "",
    data = null,
  } = props;

  const handleOnClickModal = useCallback((formType) => {
    actionMenu.setStateMenu({
      type: type + formType,
      data: data
    })
  }, [data])
  return (
    <TitleWapper>
      <LineBefore color={colorsHV[level]} bg={colors[level]} />
      {
        isLast ? null :
          <CircleBtn color={colorsHV[level]} bg={isExpand ? colors['isExpand'] : colors[level]} onClick={() => isLoading ? {} : handleClick()} >
            {isLoading ? <Spin size="small" /> : (isExpand ? <MinusOutlined /> : <PlusOutlined />)}
          </CircleBtn>
      }
      <p onClick={() => isLoading ? {} : handleClick()} style={{ margin: 0, display: 'inline', color: colorsHV[level], paddingRight: '20px' }}> {text} </p>
      <div>
        {
          !isLast ?
            <HandleBtn
              onClick={() => handleOnClickModal('add')}
              size="small"
              type="link"
              hover='#1A9CFC'
              shape="circle"
              style={{ margin: '0 5px' }}
              // shape="round"
              icon={<PlusCircleOutlined />}
            /> : null
        }
        <HandleBtn
          onClick={() => handleOnClickModal('edit')}
          style={{ margin: '0 5px' }}
          size="small"
          type="link"
          hover='#1A9CFC'
          shape="circle"
          icon={<EditOutlined />}
        />
        <HandleBtn
          onClick={() => handleOnClickModal('delete')}
          style={{ margin: '0 5px' }}
          size="small"
          type="link"
          hover='#FD4F54'
          shape="circle"
          icon={<DeleteOutlined />}
        />
      </div>
    </TitleWapper>
  )
}

const HandleBtn = styled(Button)`
  margin: 0 5px;
  &:hover { 
    background-color: ${props => props.hover};
    color: #fff
  }

`

const TitleWapper = styled.div`
display: flex;
flex-direction: row;
align-items: center;
`;

const LineBefore = styled.div`
  height: 1px;
  width: 10px;
  background: ${props => props.color};
`;

const CircleBtn = styled.div`
height: 30px;
width: 30px;
background: ${props => props.bg};
border-radius: 30px;
border: ${props => `2px solid ${props.color}`};
margin: 5px;
display: flex;
justify-content: center;
align-items: center;
`;

const SolidBorder = styled.div`
border-left: ${props => `2px solid ${props.bg || colors[1]}`};
background: #f0f2f5;
margin-left: 29px;
`;
// class =====================================================================
const dataClass = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
const MenuApp = (props) => {
  const [collapsed, setCollapsed] = useState(null);
  return (
    <SolidBorder bg={colors[1]}>
      {dataClass.map((className, index) => {
        return (
          <ClassMenu {...props} className={className} title={`Lớp ${className}`} key={String(className)} collapsed={collapsed} setCollapsed={setCollapsed} index={index} />
        )
      })}
    </SolidBorder>
  )
}

const ClassMenu = ({ title, setCollapsed, collapsed, index, className, ...props }) => {
  const [dataClass, setDataClass] = useState(null);
  const [isLoading, setIsLoading] = useState(null);
  const isOpent = collapsed === index;
  const handleCollapsed = useCallback(() => {
    if (!dataClass) {
      setIsLoading(true);
      // const api = `${baseApi}series-url/series_lop-${className}`
      //series-url/series_lop-${className}
      setTimeout(() => {
        setDataClass(topics);
        setCollapsed(collapsed === index ? null : index);
        setIsLoading(false)
      }, 1000);
    } else {
      setCollapsed(collapsed === index ? null : index);
    }
  }, [collapsed, dataClass, isLoading]);

  return (
    <div>
      <Title
        text={title}
        handleClick={handleCollapsed}
        isLoading={isLoading}
        level={1}
        isExpand={isOpent}
        data={dataClass}
        type="class"
      />
      {
        isOpent && dataClass ?
          <ClassContent data={dataClass} {...props} /> :
          null
      }
    </div>
  )
}


const ClassContent = ({ data, ...props }) => {
  const [collapsedClass, setCollapsedClass] = useState(null);
  return (
    <SolidBorder bg={colors[2]}>
      {data.map((i, index) => {
        return <SubjectMenu
          title={i.title}
          subjectContent={i.categories}
          key={String(index)}
          collapsedClass={collapsedClass}
          setCollapsedClass={setCollapsedClass}
          index={index}
          {...props}
        />
      })}
    </SolidBorder>
  )
}
// subject =====================================================================
const SubjectMenu = ({ title, subjectContent, setCollapsedClass, collapsedClass, index, ...props }) => {
  const handleCollapsed = useCallback(() => {
    setCollapsedClass(collapsedClass === index ? null : index);
  }, [collapsedClass]);

  return (
    <div>
      <Title text={title} handleClick={handleCollapsed} isExpand={collapsedClass === index} level={2}
        data={dataClass}
        type="subject" />
      {
        collapsedClass === index ?
          <SubjectContent subjectContent={subjectContent} /> :
          null
      }
    </div>
  )
}
const SubjectContent = ({ subjectContent }) => {
  const [collapsedSubject, setCollapsedSubject] = useState(null);
  return (
    <SolidBorder bg={colors[2]}>
      {subjectContent.map((subject, index) => {
        if (subject._id) { // resutrn sub subject 
          return (
            <BookMenu key={String(index)} index={index} subject={subject} bookName={subject.title} collapsedSubject={collapsedSubject} setCollapsedSubject={setCollapsedSubject} />
          )
        } else { // return 
          return (
            <div>
              <Title text={subject.title} key={String(index)} />
            </div>
          )
        }
      })}
    </SolidBorder>
  )

}
// book  =====================================================================
const BookMenu = ({ subject, bookName, index, collapsedSubject, setCollapsedSubject }) => {
  const [bookData, setBookData] = useState(null);
  const [isLoadingBook, setIsLoadingBook] = useState(null);
  const isExpandBook = index === collapsedSubject;
  const handleCollapsedBook = useCallback(() => {

    if (!bookData) {
      setIsLoadingBook(true);
      // const api = `${baseApi}series-url/series_lop-${className}`
      //series-url/series_lop-${className}
      setTimeout(() => {
        setBookData(bookMenu.children);
        setCollapsedSubject(collapsedSubject === index ? null : index);
        setIsLoadingBook(false)
      }, 1000);
    } else {
      setCollapsedSubject(collapsedSubject === index ? null : index);
    }
  })

  return (
    <div>
      <Title // sub menu
        level={2}
        text={bookName}
        handleClick={handleCollapsedBook}
        isLoading={isLoadingBook}
        isExpand={isExpandBook}
        type="book"
        data={bookData}
      />
      {
        isExpandBook && bookData ?
          <BookMenuContent
            bookContent={bookData}
            index={index} /> :
          null
      }
    </div>
  )
}

// table content  =====================================================================
const BookMenuContent = ({ bookContent }) => {
  const [collapsedBook, setCollapsedBook] = useState(null);
  return (
    <SolidBorder bg={colorsHV[3]}>
      {bookContent.map((book, index) => {
        if (book.type === 0) { // nested
          return (
            <BookMenuNested key={String(index)} book={book} index={index} collapsedBook={collapsedBook} setCollapsedBook={setCollapsedBook} />
          )
        } else { // end
          return (
            <Title level={3} text={book.title} key={String(index)} isLast />
          )
        }
      })}
    </SolidBorder>
  )
}
const BookMenuNested = ({ book, index, collapsedBook, setCollapsedBook }) => {
  return (
    <div>
      <Title
        level={3}
        text={book.title}
        handleClick={() => {
          setCollapsedBook(collapsedBook === index ? null : index)
        }}
        isExpand={collapsedBook === index}
      />
      {
        collapsedBook === index ? <BookMenuContent bookContent={book.children} /> : null
      }

    </div>

  )
}


const topics = [
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265fd",
        category_id: "5d99f87efa14c2021592658e",
        title: "Soạn Văn 10 (hay nhất)",
        url: "soan-van-lop-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265fc",
        category_id: "5d99f87efa14c2021592658f",
        title: "Soạn Văn 10 (ngắn nhất)",
        url: "soan-van-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265fb",
        category_id: "5d99f87efa14c20215926590",
        title: "Soạn Văn 10 (siêu ngắn)",
        url: "ngu-van-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265fa",
        category_id: "5d99f87efa14c20215926591",
        title: "Soạn Văn lớp 10 (cực ngắn)",
        url: "soan-bai-lop-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265f9",
        category_id: "5d99f87efa14c20215926592",
        title: "Văn mẫu lớp 10",
        url: "van-mau-lop-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265f8",
        category_id: "5d99f87efa14c20215926593",
        title: "Tác giả - Tác phẩm Văn 10",
        url: "ngu-van-10_tac-gia-tac-pham-lop-10",
        group: 2,
        group_name: "khác",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265f7",
        category_id: "5d99f87efa14c20215926594",
        title: "Lý thuyết, Bài tập Tiếng việt - Tập làm văn 10",
        url: "ngu-van-10_ly-thuyet-ngu-van-10",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5e0f22320fdd69004e5fc2eb",
        category_id: "5e0f22320fdd69004e5fc2bb",
        title: "Đề kiểm tra Ngữ Văn 10 (có đáp án)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-ngu-van-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5e0f22320fdd69004e5fc2ea",
        category_id: "5e0f22320fdd69004e5fc2bc",
        title: "Giáo án Ngữ văn 10 chuẩn",
        url: "giao-an-ngu-van-10_index",
        group: 2,
        group_name: "khác",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265f6",
    title: "Môn Ngữ văn",
    type: 2,
    subject_id: 2
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265f5",
        category_id: "5d99f87efa14c20215926595",
        title: "Giải bài tập Toán 10",
        url: "giai-toan-lop-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265f4",
        category_id: "5d99f87efa14c20215926596",
        title: "Giải bài tập Toán 10 nâng cao",
        url: "giai-toan-10-nang-cao_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265f3",
        category_id: "5d99f87efa14c20215926597",
        title: "Giải sách bài tập Toán 10",
        url: "giai-sach-bai-tap-toan-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5e8bed23f0a3a000a47be637",
        category_id: "5e8bed23f0a3a000a47be604",
        title: "Đề kiểm tra Toán 10 (có đáp án)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-toan-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265f2",
        category_id: "5d99f87efa14c20215926598",
        title: "Chuyên đề Toán 10 (có đáp án - cực hay)",
        url: "toan-lop-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265f1",
        category_id: "5d99f87efa14c20215926599",
        title: "Bài tập trắc nghiệm Hình học 10 (50 đề)",
        url: "bai-tap-trac-nghiem-hinh-hoc-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265f0",
    title: "Môn Toán",
    type: 3,
    subject_id: 1
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265ef",
        category_id: "5d99f87efa14c2021592659a",
        title: "Giải bài tập Vật Lí 10",
        url: "giai-bai-tap-vat-ly-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265ee",
        category_id: "5d99f87efa14c2021592659b",
        title: "Giải bài tập Vật Lí 10 nâng cao",
        url: "giai-ly-10-nang-cao_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265ed",
        category_id: "5d99f87efa14c2021592659c",
        title: "Giải sách bài tập Vật Lí 10",
        url: "giai-sach-bai-tap-vat-li-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265ec",
        category_id: "5d99f87efa14c2021592659d",
        title: "Chuyên đề Vật Lí 10 (có đáp án - cực hay)",
        url: "vat-ly-lop-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265eb",
        category_id: "5d99f87efa14c2021592659e",
        title: "Bài tập trắc nghiệm Vật Lí 10 (70 đề)",
        url: "bai-tap-trac-nghiem-vat-li-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5e8bed23f0a3a000a47be638",
        category_id: "5e8bed23f0a3a000a47be60c",
        title: "Đề kiểm tra Vật Lí 10 (có đáp án)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-vat-li-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265ea",
    title: "Môn Vật Lí",
    type: 11,
    subject_id: 3
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265e9",
        category_id: "5d99f87efa14c2021592659f",
        title: "Giải bài tập Hóa học 10",
        url: "giai-hoa-lop-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265e8",
        category_id: "5d99f87efa14c202159265a0",
        title: "Giải bài tập Hóa học 10 nâng cao",
        url: "giai-bai-tap-hoa-10-nang-cao_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265e7",
        category_id: "5d99f87efa14c202159265a1",
        title: "Giải sách bài tập Hóa 10",
        url: "giai-sach-bai-tap-hoa-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265e6",
        category_id: "5d99f87efa14c202159265a2",
        title: "Wiki Tính chất hóa học",
        url: "tinh-chat-hoa-hoc_index",
        group: 2,
        group_name: "khác",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265e5",
        category_id: "5d99f87efa14c202159265a3",
        title: "Wiki Phản ứng hóa học",
        url: "phan-ung-hoa-hoc_index",
        group: 2,
        group_name: "khác",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265e4",
        category_id: "5d99f87efa14c202159265a4",
        title: "Chuyên đề Hóa học 10 (có đáp án - cực hay)",
        url: "hoa-hoc-lop-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265e3",
        category_id: "5d99f87efa14c202159265a5",
        title: "Bài tập trắc nghiệm Hóa 10 (70 đề)",
        url: "bai-tap-trac-nghiem-hoa-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265e2",
        category_id: "5d99f87efa14c202159265a6",
        title: "Đề kiểm tra Hóa học 10 (100 đề)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-hoa-hoc-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5e0f22320fdd69004e5fc2ec",
        category_id: "5e0f22320fdd69004e5fc2cf",
        title: "11 phương pháp giải nhanh Hóa học hữu cơ, vô cơ cực hay",
        url: "hoa-hoc-lop-12_cac-phuong-phap-giai-nhanh-hoa-hoc-huu-co-vo-co",
        group: 2,
        group_name: "khác",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265e1",
    title: "Môn Hóa học",
    type: 15,
    subject_id: 4
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265e0",
        category_id: "5d99f87efa14c202159265a7",
        title: "Giải bài tập Sinh học 10",
        url: "giai-bai-tap-sinh-hoc-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265df",
        category_id: "5d99f87efa14c202159265a8",
        title: "Giải bài tập Sinh 10 (ngắn nhất)",
        url: "giai-bai-tap-sinh-10_index",
        group: 2,
        group_name: "khác",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265de",
        category_id: "5d99f87efa14c202159265a9",
        title: "Giải bài tập Sinh 10 nâng cao",
        url: "giai-bai-tap-sinh-10-nang-cao_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265dd",
        category_id: "5d99f87efa14c202159265aa",
        title: "Bài tập trắc nghiệm Sinh học 10 (35 đề)",
        url: "bai-tap-trac-nghiem-sinh-hoc-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265dc",
    title: "Môn Sinh học",
    type: 12,
    subject_id: 8
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265db",
        category_id: "5d99f87efa14c202159265ab",
        title: "Giải bài tập Địa Lí 10",
        url: "giai-bai-tap-dia-li-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265da",
        category_id: "5d99f87efa14c202159265ac",
        title: "Giải bài tập Địa Lí 10 (ngắn nhất)",
        url: "giai-dia-li-lop-10_index",
        group: 2,
        group_name: "khác",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265d9",
        category_id: "5d99f87efa14c202159265ad",
        title: "Giải Tập bản đồ và bài tập thực hành Địa Lí 10",
        url: "giai-tap-ban-do-va-bai-tap-thuc-hanh-dia-li-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265d8",
        category_id: "5d99f87efa14c202159265ae",
        title: "Bài tập trắc nghiệm Địa Lí 10 (50 đề)",
        url: "bai-tap-trac-nghiem-dia-li-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265d7",
        category_id: "5d99f87efa14c202159265af",
        title: "Đề kiểm tra Địa Lí 10 (100 đề)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-dia-li-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265d6",
    title: "Môn Địa Lí",
    type: 10,
    subject_id: 7
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265d5",
        category_id: "5d99f87efa14c202159265b0",
        title: "Giải bài tập Tiếng anh 10",
        url: "tieng-anh-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265d4",
        category_id: "5d99f87efa14c202159265b1",
        title: "Giải sách bài tập Tiếng Anh 10",
        url: "giai-sach-bai-tap-tieng-anh-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5e0f22320fdd69004e5fc2ed",
        category_id: "5e0f22320fdd69004e5fc2db",
        title: "960 Bài tập trắc nghiệm Tiếng anh 10 có đáp án",
        url: "trac-nghiem-tieng-anh-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5e8bed23f0a3a000a47be63b",
        category_id: "5e8bed23f0a3a000a47be623",
        title: "Đề kiểm tra Tiếng Anh 10",
        url: "de-kiem-tra-lop-10_de-kiem-tra-tieng-anh-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265d3",
        category_id: "5d99f87efa14c202159265b2",
        title: "Giải bài tập Tiếng anh 10 thí điểm",
        url: "tieng-anh-10-moi_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265d2",
        category_id: "5d99f87efa14c202159265b3",
        title: "Giải sách bài tập Tiếng Anh 10 mới",
        url: "giai-sach-bai-tap-tieng-anh-10-moi_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5e8bed23f0a3a000a47be63a",
        category_id: "5e8bed23f0a3a000a47be626",
        title: "Từ vựng, Ngữ pháp, Bài tập Tiếng Anh 10 có đáp án",
        url: "bai-tap-tieng-anh-10_index",
        group: 2,
        group_name: "khác",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265d1",
        category_id: "5d99f87efa14c202159265b4",
        title: "720 Bài tập trắc nghiệm Tiếng anh 10 mới có đáp án",
        url: "trac-nghiem-tieng-anh-10-moi_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5e8bed23f0a3a000a47be639",
        category_id: "5e8bed23f0a3a000a47be628",
        title: "Đề kiểm tra Tiếng Anh 10 mới",
        url: "de-kiem-tra-lop-10_de-kiem-tra-tieng-anh-10-thi-diem",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265d0",
    title: "Môn Tiếng anh",
    type: 4,
    subject_id: 5
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265cf",
        category_id: "5d99f87efa14c202159265b5",
        title: "Giải bài tập Lịch sử 10",
        url: "giai-bai-tap-lich-su-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265ce",
        category_id: "5d99f87efa14c202159265b6",
        title: "Giải bài tập Lịch sử 10 (ngắn nhất)",
        url: "giai-lich-su-lop-10_index",
        group: 2,
        group_name: "khác",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265cd",
        category_id: "5d99f87efa14c202159265b7",
        title: "Giải tập bản đồ Lịch sử 10",
        url: "giai-tap-ban-do-lich-su-lop-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265cc",
        category_id: "5d99f87efa14c202159265b8",
        title: "Đề kiểm tra Lịch Sử 10 (100 đề)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-lich-su-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5d99f87efa14c202159265cb",
        category_id: "5d99f87efa14c202159265b9",
        title: "Bài tập trắc nghiệm Lịch Sử (50 đề) 10",
        url: "bai-tap-trac-nghiem-lich-su-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5ec25a8f4cc2f0002c0e7d46",
        category_id: "5ec25a8f4cc2f0002c0e7d3b",
        title: "Giáo án Lịch Sử 10 chuẩn",
        url: "giao-an-lich-su-10_index",
        group: 2,
        group_name: "khác",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265ca",
    title: "Môn Lịch sử",
    type: 9,
    subject_id: 6
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265c9",
        category_id: "5d99f87efa14c202159265ba",
        title: "Giải bài tập Tin học 10",
        url: "giai-bai-tap-tin-hoc-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5e8bed23f0a3a000a47be63c",
        category_id: "5e8bed23f0a3a000a47be62f",
        title: "Đề kiểm tra Tin học 10 có đáp án",
        url: "de-kiem-tra-lop-10_de-kiem-tra-tin-hoc-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265c8",
    title: "Môn Tin học",
    type: 7,
    subject_id: 0
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265c7",
        category_id: "5d99f87efa14c202159265bb",
        title: "Giải bài tập GDCD 10",
        url: "giai-bai-tap-giao-duc-cong-dan-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265c6",
        category_id: "5d99f87efa14c202159265bc",
        title: "Giải bài tập GDCD 10 (ngắn nhất)",
        url: "giai-bai-tap-gdcd-10_index",
        group: 2,
        group_name: "khác",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265c5",
        category_id: "5d99f87efa14c202159265bd",
        title: "Bài tập trắc nghiệm GDCD 10 (38 đề)",
        url: "bai-tap-trac-nghiem-gdcd-10_index",
        group: 2,
        group_name: "chuyên đề & trắc nghiệm",
        priority: 0
      },
      {
        _id: "5e8bed23f0a3a000a47be63d",
        category_id: "5e8bed23f0a3a000a47be633",
        title: "Đề kiểm tra GDCD 10 có đáp án",
        url: "de-kiem-tra-lop-10_de-kiem-tra-giao-duc-cong-dan-lop-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      },
      {
        _id: "5ec25a8f4cc2f0002c0e7d47",
        category_id: "5ec25a8f4cc2f0002c0e7d42",
        title: "Giáo án GDCD 10 chuẩn",
        url: "giao-an-gdcd-10_index",
        group: 2,
        group_name: "khác",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265c4",
    title: "Môn GDCD",
    type: 13,
    subject_id: 10
  },
  {
    categories: [
      {
        _id: "5d99f87efa14c202159265c3",
        category_id: "5d99f87efa14c202159265be",
        title: "Giải bài tập Công nghệ 10",
        url: "giai-bai-tap-cong-nghe-10_index",
        group: 1,
        group_name: "sách giáo khoa",
        priority: 10
      },
      {
        _id: "5d99f87efa14c202159265c2",
        category_id: "5d99f87efa14c202159265bf",
        title: "Giải Sách bài tập Công nghệ 10",
        url: "giai-sach-bai-tap-cong-nghe-10_index",
        group: 2,
        group_name: "sách/vở bài tập",
        priority: 10
      },
      {
        _id: "5e8bed23f0a3a000a47be63e",
        category_id: "5e8bed23f0a3a000a47be636",
        title: "Đề kiểm tra Công nghệ 10 (có đáp án)",
        url: "de-kiem-tra-lop-10_de-kiem-tra-cong-nghe-lop-10",
        group: 2,
        group_name: "đề kiểm tra",
        priority: 0
      }
    ],
    _id: "5d99f87efa14c202159265c1",
    title: "Môn Công nghệ",
    type: 14,
    subject_id: 9
  }
];

const bookMenu = {
  _id: "5d99f87efa14c2021592641e",
  url: "soan-van-7_index",
  series_url: "series_lop-7",
  topic_id: "5d99f87efa14c20215926489",
  title: "Soạn Văn 7 (ngắn nhất)",
  text: "",
  type: 0,
  group: 1,
  group_name: "sách giáo khoa",
  children: [
    {
      children: [
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc574a2",
              title: "Cổng trường mở ra",
              url: "soan-van-7_cong-truong-mo-ra",
              type: 1,
              html_title: "Soạn bài Cổng trường mở ra | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc574a1",
              title: "Mẹ tôi",
              url: "soan-van-7_me-toi",
              type: 1,
              html_title: "Soạn bài Mẹ tôi | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc574a0",
              title: "Từ ghép",
              url: "soan-van-7_tu-ghep",
              type: 1,
              html_title: "Soạn bài Từ ghép | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5749f",
              title: "Liên kết trong văn bản",
              url: "soan-van-7_lien-ket-trong-van-ban",
              type: 1,
              html_title: "Soạn bài Liên kết trong văn bản | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5749e",
          title: "Bài 1",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5749d",
              title: "Cuộc chia tay của những con búp bê",
              url: "soan-van-7_cuoc-chia-tay-cua-nhung-con-bup-be",
              type: 1,
              html_title: "Soạn bài Cuộc chia tay của những con búp bê | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5749c",
              title: "Bố cục trong văn bản",
              url: "soan-van-7_bo-cuc-trong-van-ban",
              type: 1,
              html_title: "Soạn bài Bố cục trong văn bản | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5749b",
              title: "Mạch lạc trong văn bản",
              url: "soan-van-7_mach-lac-trong-van-ban",
              type: 1,
              html_title: "Soạn bài Mạch lạc trong văn bản | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5749a",
          title: "Bài 2",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57499",
              title: "Ca dao, dân ca những câu hát về tình cảm gia đình",
              url: "soan-van-7_ca-dao-dan-ca-nhung-cau-hat-ve-tinh-cam-gia-dinh",
              type: 1,
              html_title: "Soạn bài Ca dao, dân ca những câu hát về tình cảm gia đình | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57498",
              title: "Những câu hát về tình yêu quê hương, đất nước, con người",
              url: "soan-van-7_nhung-cau-hat-ve-tinh-yeu-que-huong-dat-nuoc-con-nguoi",
              type: 1,
              html_title: "Soạn bài Những câu hát về tình yêu quê hương, đất nước, con người | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57497",
              title: "Từ láy",
              url: "soan-van-7_tu-lay",
              type: 1,
              html_title: "Soạn bài Từ láy | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57496",
              title: "Viết bài tập làm văn số 1 - Văn tự sự và miêu tả",
              url: "soan-van-7_viet-bai-tap-lam-van-so-1-van-tu-su-va-mieu-ta",
              type: 1,
              html_title: "Soạn bài Viết bài tập làm văn số 1 - Văn tự sự và miêu tả | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57495",
              title: "Quá trình tạo lập văn bản",
              url: "soan-van-7_qua-trinh-tao-lap-van-ban",
              type: 1,
              html_title: "Soạn bài Quá trình tạo lập văn bản | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57494",
          title: "Bài 3",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57493",
              title: "Những câu hát than thân",
              url: "soan-van-7_nhung-cau-hat-than-than",
              type: 1,
              html_title: "Soạn bài Những câu hát than thân | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57492",
              title: "Những câu hát châm biếm",
              url: "soan-van-7_nhung-cau-hat-cham-biem",
              type: 1,
              html_title: "Soạn bài Những câu hát châm biếm | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57491",
              title: "Đại từ",
              url: "soan-van-7_dai-tu",
              type: 1,
              html_title: "Soạn bài Đại từ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57490",
              title: "Luyện tập tạo lập văn bản",
              url: "soan-van-7_luyen-tap-tao-lap-van-ban",
              type: 1,
              html_title: "Soạn bài Luyện tập tạo lập văn bản | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5748f",
          title: "Bài 4",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5748e",
              title: "Sông núi nước Nam",
              url: "soan-van-7_song-nui-nuoc-nam",
              type: 1,
              html_title: "Soạn bài Sông núi nước Nam | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5748d",
              title: "Phò giá về kinh",
              url: "soan-van-7_pho-gia-ve-kinh",
              type: 1,
              html_title: "Soạn bài Phò giá về kinh | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5748c",
              title: "Từ hán việt",
              url: "soan-van-7_tu-han-viet",
              type: 1,
              html_title: "Soạn bài Từ hán việt | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5748b",
              title: "Trả bài tập làm văn số 1",
              url: "soan-van-7_tra-bai-tap-lam-van-so-1",
              type: 1,
              html_title: "Soạn bài Trả bài tập làm văn số 1 | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5748a",
              title: "Tìm hiểu chung về văn biểu cảm",
              url: "soan-van-7_tim-hieu-chung-ve-van-ban-bieu-cam",
              type: 1,
              html_title: "Soạn bài Tìm hiểu chung về văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57489",
          title: "Bài 5",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57488",
              title: "Buổi chiều đứng ở phủ Thiên Trường trông ra",
              url: "soan-van-7_buoi-chieu-dung-o-phu-thien-truong-trong-ra",
              type: 1,
              html_title: "Soạn bài Buổi chiều đứng ở phủ Thiên Trường trông ra | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57487",
              title: "Bài ca Côn Sơn",
              url: "soan-van-7_bai-ca-con-son",
              type: 1,
              html_title: "Soạn bài Bài ca Côn Sơn | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57486",
              title: "Từ hán việt (tiếp theo)",
              url: "soan-van-7_tu-han-viet-tiep-theo",
              type: 1,
              html_title: "Soạn bài Từ hán việt (tiếp theo) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57485",
              title: "Đặc điểm của văn bản biểu cảm",
              url: "soan-van-7_dac-diem-cua-van-ban-bieu-cam",
              type: 1,
              html_title: "Soạn bài Đặc điểm của văn bản biểu cảm | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57484",
              title: "Đề văn biểu cảm và cách làm bài văn biểu cảm",
              url: "soan-van-7_de-van-bieu-cam-va-cach-lam-bai-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Đề văn biểu cảm và cách làm bài văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57483",
          title: "Bài 6",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57482",
              title: "Sau phút chia li",
              url: "soan-van-7_sau-phut-chia-li",
              type: 1,
              html_title: "Soạn bài Sau phút chia li | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57481",
              title: "Bánh trôi nước",
              url: "soan-van-7_banh-troi-nuoc",
              type: 1,
              html_title: "Soạn bài Bánh trôi nước | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57480",
              title: "Quan hệ từ",
              url: "soan-van-7_quan-he-tu",
              type: 1,
              html_title: "Soạn bài Quan hệ từ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5747f",
              title: "Luyên tập cách làm văn biểu cảm",
              url: "soan-van-7_luyen-tap-cach-lam-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Luyên tập cách làm văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5747e",
          title: "Bài 7",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5747d",
              title: "Qua đèo ngang",
              url: "soan-van-7_qua-deo-ngang",
              type: 1,
              html_title: "Soạn bài Qua đèo ngang | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5747c",
              title: "Bạn đến chơi nhà",
              url: "soan-van-7_ban-den-choi-nha",
              type: 1,
              html_title: "Soạn bài Bạn đến chơi nhà | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5747b",
              title: "Chữa lỗi về quan hệ từ",
              url: "soan-van-7_chua-loi-ve-quan-he-tu",
              type: 1,
              html_title: "Soạn bài Chữa lỗi về quan hệ từ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5747a",
              title: "Viết bài tập làm văn số 2 - Văn biểu cảm",
              url: "soan-van-7_viet-bai-tap-lam-van-so-2-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Viết bài tập làm văn số 2 - Văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57479",
          title: "Bài 8",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57478",
              title: "Xa ngắm thác núi Lư",
              url: "soan-van-7_xa-ngam-thac-nui-lu",
              type: 1,
              html_title: "Soạn bài Xa ngắm thác núi Lư | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57477",
              title: "Từ đồng nghĩa",
              url: "soan-van-7_tu-dong-nghia",
              type: 1,
              html_title: "Soạn bài Từ đồng nghĩa | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57476",
              title: "Cách lập ý của bài văn biểu cảm",
              url: "soan-van-7_cach-lap-y-cua-bai-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Cách lập ý của bài văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57475",
          title: "Bài 9",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57474",
              title: "Cảm nghĩ trong đêm thanh tĩnh",
              url: "soan-van-7_cam-nghi-trong-dem-thanh-tinh",
              type: 1,
              html_title: "Soạn bài Cảm nghĩ trong đêm thanh tĩnh | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57473",
              title: "Ngẫu nhiên viết nhân buổi mới về quê",
              url: "soan-van-7_ngau-nhien-viet-nhan-buoi-moi-ve-que",
              type: 1,
              html_title: "Soạn bài Ngẫu nhiên viết nhân buổi mới về quê | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57472",
              title: "Từ trái nghĩa",
              url: "soan-van-7_tu-trai-nghia",
              type: 1,
              html_title: "Soạn bài Từ trái nghĩa | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57471",
              title: "Luyện nói: văn biểu cảm về sự vật, con người",
              url: "soan-van-7_luyen-noi-van-bieu-cam-ve-su-vat-con-nguoi",
              type: 1,
              html_title: "Soạn bài Luyện nói: văn biểu cảm về sự vật, con người | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57470",
          title: "Bài 10",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5746f",
              title: "Bài ca nhà tranh bị gió thu phá",
              url: "soan-van-7_bai-ca-nha-tranh-bi-gio-thu-pha",
              type: 1,
              html_title: "Soạn bài Bài ca nhà tranh bị gió thu phá | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5746e",
              title: "Từ đồng âm",
              url: "soan-van-7_tu-dong-am",
              type: 1,
              html_title: "Soạn bài Từ đồng âm | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5746d",
              title: "Trả bài tập làm văn số 2",
              url: "soan-van-7_tra-bai-tap-lam-van-so-2",
              type: 1,
              html_title: "Soạn bài Trả bài tập làm văn số 2 | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5746c",
              title: "Các yếu tố tự sự, miêu tả trong văn bản biểu cảm",
              url: "soan-van-7_cac-yeu-to-tu-su-mieu-ta-trong-van-ban-bieu-cam",
              type: 1,
              html_title: "Soạn bài Các yếu tố tự sự, miêu tả trong văn bản biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5746b",
          title: "Bài 11",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5746a",
              title: "Cảnh khuya, Rằm tháng giêng",
              url: "soan-van-7_canh-khuya-ram-thang-gieng",
              type: 1,
              html_title: "Soạn bài Cảnh khuya, Rằm tháng giêng | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57469",
              title: "Thành ngữ",
              url: "soan-van-7_thanh-ngu",
              type: 1,
              html_title: "Soạn bài Thành ngữ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57468",
              title: "Viết bài tập làm văn số 3 - Văn biểu cảm",
              url: "soan-van-7_viet-bai-tap-lam-van-so-3-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Viết bài tập làm văn số 3 - Văn biểu cảm | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57467",
              title: "Cách làm bài văn biểu cảm về tác phẩm văn học",
              url: "soan-van-7_cach-lam-bai-van-bieu-cam-ve-tac-pham-van-hoc",
              type: 1,
              html_title: "Soạn bài Cách làm bài văn biểu cảm về tác phẩm văn học | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57466",
          title: "Bài 12",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57465",
              title: "Tiếng gà trưa",
              url: "soan-van-7_tieng-ga-trua",
              type: 1,
              html_title: "Soạn bài Tiếng gà trưa | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57464",
              title: "Điệp ngữ",
              url: "soan-van-7_diep-ngu",
              type: 1,
              html_title: "Soạn bài Điệp ngữ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57463",
              title: "Luyện nói: Phát biểu cảm nghĩ về tác phẩm văn học",
              url: "soan-van-7_luyen-noi-phat-bieu-cam-nghi-ve-tac-pham-van-hoc",
              type: 1,
              html_title: "Soạn bài Luyện nói: Phát biểu cảm nghĩ về tác phẩm văn học | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57462",
              title: "Làm thơ lục bát",
              url: "soan-van-7_lam-tho-luc-bat",
              type: 1,
              html_title: "Soạn bài Làm thơ lục bát | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57461",
          title: "Bài 13",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57460",
              title: "Một thứ quà của lúa non: Cốm",
              url: "soan-van-7_mot-thu-qua-cua-lua-non-com",
              type: 1,
              html_title: "Soạn bài Một thứ quà của lúa non: Cốm | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5745f",
              title: "Chơi chữ",
              url: "soan-van-7_choi-chu",
              type: 1,
              html_title: "Soạn bài Chơi chữ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5745e",
              title: "Chuẩn mực sử dụng từ",
              url: "soan-van-7_chuan-muc-su-dung-tu",
              type: 1,
              html_title: "Soạn bài Chuẩn mực sử dụng từ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5745d",
              title: "Ôn tập văn biểu cảm",
              url: "soan-van-7_on-tap-van-bieu-cam",
              type: 1,
              html_title: "Soạn bài Ôn tập văn biểu cảm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5745c",
          title: "Bài 14",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5745b",
              title: "Sài Gòn tôi yêu",
              url: "soan-van-7_sai-gon-toi-yeu",
              type: 1,
              html_title: "Soạn bài Sài Gòn tôi yêu | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5745a",
              title: "Mùa xuân của tôi",
              url: "soan-van-7_mua-xuan-cua-toi",
              type: 1,
              html_title: "Soạn bài Mùa xuân của tôi | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57459",
              title: "Luyện tập sử dụng từ",
              url: "soan-van-7_luyen-tap-su-dung-tu",
              type: 1,
              html_title: "Soạn bài Luyện tập sử dụng từ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57458",
              title: "Trả bài tập làm văn số 3",
              url: "soan-van-7_tra-bai-tap-lam-van-so-3",
              type: 1,
              html_title: "Soạn bài Trả bài tập làm văn số 3 | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57457",
          title: "Bài 15",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57456",
              title: "Ôn tập tác phẩm trữ tình",
              url: "soan-van-7_on-tap-tac-pham-tru-tinh",
              type: 1,
              html_title: "Soạn bài Ôn tập tác phẩm trữ tình | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57455",
              title: "Ôn tập phần tiếng việt",
              url: "soan-van-7_on-tap-phan-tieng-viet-tap-1",
              type: 1,
              html_title: "Soạn bài Ôn tập phần tiếng việt | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57454",
              title: "Kiểm tra tổng hợp cuối học kì 1",
              url: "soan-van-7_kiem-tra-tong-hop-cuoi-hoc-ki-1",
              type: 1,
              html_title: "Soạn bài Kiểm tra tổng hợp cuối học kì 1 | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57453",
          title: "Bài 16",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57452",
              title: "Ôn tập tác phẩm trữ tình (tiếp theo)",
              url: "soan-van-7_on-tap-tac-pham-tru-tinh-tiep-theo",
              type: 1,
              html_title: "Soạn bài Ôn tập tác phẩm trữ tình (tiếp theo) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57451",
              title: "Ôn tập phần tiếng việt (tiếp theo)",
              url: "soan-van-7_on-tap-phan-tieng-viet-tiep-theo",
              type: 1,
              html_title: "Soạn bài Ôn tập phần tiếng việt (tiếp theo - kì 1) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57450",
              title: "Chương trình địa phương: Rèn luyện chính tả (phần tiếng việt)",
              url: "soan-van-7_chuong-trinh-dia-phuong-phan-tieng-viet-ren-luyen-chinh-ta",
              type: 1,
              html_title: "Soạn bài Chương trình địa phương phần Tiếng Việt: Rèn luyện chính tả | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5744f",
          title: "Bài 17",
          url: "",
          type: 0,
          html_title: ""
        }
      ],
      _id: "5de9d6e5843e27002cc5744e",
      title: "Soạn văn 7 Tập 1",
      url: "soan-van-7_soan-van-7-tap-1",
      type: 0,
      html_title: "Soạn văn lớp 7 Tập 1 ngắn nhất | Soạn bài lớp 7 Tập 1"
    },
    {
      children: [
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5744d",
              title: "Tục ngữ về thiên nhiên và lao động sản xuất",
              url: "soan-van-7_tuc-ngu-ve-thien-nhien-va-lao-dong-san-xuat",
              type: 1,
              html_title: "Soạn bài Tục ngữ về thiên nhiên và lao động sản xuất | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5744c",
              title: "Chương trình địa phương (phần Văn và Tập làm văn)",
              url: "soan-van-7_chuong-trinh-dia-phuong-bai-18",
              type: 1,
              html_title: "Soạn bài Chương trình địa phương phần Văn và Tập làm văn | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5744b",
              title: "Tìm hiểu chung về văn nghị luận",
              url: "soan-van-7_tim-hieu-chung-ve-van-nghi-luan",
              type: 1,
              html_title: "Soạn bài Tìm hiểu chung về văn nghị luận | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5744a",
          title: "Bài 18",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57449",
              title: "Tục ngữ về con người và xã hội",
              url: "soan-van-7_tuc-ngu-ve-con-nguoi-va-xa-hoi",
              type: 1,
              html_title: "Soạn bài Tục ngữ về con người và xã hội | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57448",
              title: "Rút gọn câu",
              url: "soan-van-7_rut-gon-cau",
              type: 1,
              html_title: "Soạn bài Rút gọn câu | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57447",
              title: "Đặc điểm của văn bản nghị luận",
              url: "soan-van-7_dac-diem-cua-van-ban-nghi-luan",
              type: 1,
              html_title: "Soạn bài Đặc điểm của văn bản nghị luận | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57446",
              title: "Đề văn nghị luận và việc lập ý cho bài văn nghị luận",
              url: "soan-van-7_de-van-nghi-luan-va-viec-lap-dan-y-cho-van-ban-nghi-luan",
              type: 1,
              html_title: "Soạn bài Đề văn nghị luận và việc lập ý cho bài văn nghị luận | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57445",
          title: "Bài 19",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57444",
              title: "Tinh thần yêu nước của nhân dân ta",
              url: "soan-van-7_tinh-than-yeu-nuoc-cua-nhan-dan-ta",
              type: 1,
              html_title: "Soạn bài Tinh thần yêu nước của nhân dân ta | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57443",
              title: "Câu đặc biệt",
              url: "soan-van-7_cau-dac-biet",
              type: 1,
              html_title: "Soạn bài Câu đặc biệt | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57442",
              title: "Bố cục và phương pháp lập luận trong bài văn nghị luận",
              url: "soan-van-7_bo-cuc-va-phuong-phap-lap-luan-trong-bai-van-nghi-luan",
              type: 1,
              html_title: "Soạn bài Bố cục và phương pháp lập luận trong bài văn nghị luận | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57441",
              title: "Luyện tập về phương pháp lập luận trong văn nghị luận",
              url: "soan-van-7_luyen-tap-ve-phuong-phap-lap-luan-trong-van-nghi-luan",
              type: 1,
              html_title: "Soạn bài Luyện tập về phương pháp lập luận trong văn nghị luận | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57440",
          title: "Bài 20",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5743f",
              title: "Sự giàu đẹp của tiếng việt",
              url: "soan-van-7_su-giau-dep-cua-tieng-viet",
              type: 1,
              html_title: "Soạn bài Sự giàu đẹp của tiếng việt | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5743e",
              title: "Thêm trạng ngữ cho câu",
              url: "soan-van-7_them-trang-ngu-cho-cau",
              type: 1,
              html_title: "Soạn bài Thêm trạng ngữ cho câu | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5743d",
              title: "Tìm hiểu chung về phép lập luận chứng minh",
              url: "soan-van-7_tim-hieu-chung-ve-phep-lap-luan-chung-minh",
              type: 1,
              html_title: "Soạn bài Tìm hiểu chung về phép lập luận chứng minh | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5743c",
          title: "Bài 21",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5743b",
              title: "Thêm trạng ngữ cho câu (tiếp theo)",
              url: "soan-van-7_them-trang-ngu-cho-cau-tiep-theo",
              type: 1,
              html_title: "Soạn bài Thêm trạng ngữ cho câu (tiếp theo) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5743a",
              title: "Cách làm văn lập luận chứng minh",
              url: "soan-van-7_cach-lam-bai-van-lap-luan-chung-minh",
              type: 1,
              html_title: "Soạn bài Cách làm văn lập luận chứng minh | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57439",
              title: "Luyện tập lập luận chứng minh",
              url: "soan-van-7_luyen-tap-lap-luan-chung-minh",
              type: 1,
              html_title: "Soạn bài Luyện tập lập luận chứng minh | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57438",
          title: "Bài 22",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57437",
              title: "Đức tính giản dị của Bác Hồ",
              url: "soan-van-7_duc-tinh-gian-di-cua-bac-ho",
              type: 1,
              html_title: "Soạn bài Đức tính giản dị của Bác Hồ | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57436",
              title: "Chuyển đổi câu chủ động thành câu bị động",
              url: "soan-van-7_chuyen-doi-cau-chu-dong-thanh-cau-bi-dong",
              type: 1,
              html_title: "Soạn bài Chuyển đổi câu chủ động thành câu bị động | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57435",
              title: "Viết bài tập làm văn số 5: Văn lập luận chứng minh",
              url: "soan-van-7_viet-bai-tap-lam-van-so-5-van-lap-luan-chung-minh",
              type: 1,
              html_title: "Soạn bài Viết bài tập làm văn số 5: Văn lập luận chứng minh | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57434",
          title: "Bài 23",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57433",
              title: "Ý nghĩa của văn chương",
              url: "soan-van-7_y-nghia-van-chuong",
              type: 1,
              html_title: "Soạn bài Ý nghĩa của văn chương | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57432",
              title: "Chuyển đổi câu chủ động thành câu bị động (tiếp theo)",
              url: "soan-van-7_chuyen-doi-cau-chu-dong-thanh-cau-bi-dong-tiep-theo",
              type: 1,
              html_title: "Soạn bài Chuyển đổi câu chủ động thành câu bị động (tiếp theo) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57431",
              title: "Luyện tập viết đoạn văn chứng minh",
              url: "soan-van-7_luyen-tap-viet-doan-van-chung-minh",
              type: 1,
              html_title: "Soạn bài Luyện tập viết đoạn văn chứng minh | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57430",
          title: "Bài 24",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5742f",
              title: "Ôn tập văn nghị luận",
              url: "soan-van-7_on-tap-van-nghi-luan",
              type: 1,
              html_title: "Soạn bài Ôn tập văn nghị luận | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5742e",
              title: "Dùng cụm chủ vị để mở rộng câu",
              url: "soan-van-7_dung-cum-chu-vi-de-mo-rong",
              type: 1,
              html_title: "Soạn bài Dùng cụm chủ vị để mở rộng câu | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5742d",
              title: "Trả bài tập làm văn số 5",
              url: "soan-van-7_tra-bai-tap-lam-van-so-5",
              type: 1,
              html_title: "Soạn bài Trả bài tập làm văn số 5 | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5742c",
              title: "Tìm hiểu chung về phép lập luận giải thích",
              url: "soan-van-7_tim-hieu-chung-ve-phep-lap-luan-giai-thich",
              type: 1,
              html_title: "Soạn bài Tìm hiểu chung về phép lập luận giải thích | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5742b",
          title: "Bài 25",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5742a",
              title: "Sống chết mặc bay",
              url: "soan-van-7_song-chet-mac-bay",
              type: 1,
              html_title: "Soạn bài Sống chết mặc bay | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57429",
              title: "Cách làm bài văn lập luận giải thích",
              url: "soan-van-7_cach-lam-bai-van-lap-luan-giai-thich",
              type: 1,
              html_title: "Soạn bài Cách làm bài văn lập luận giải thích | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57428",
              title: "Luyện tập lập luận giải thích",
              url: "soan-van-7_luyen-tap-lap-luan-giai-thich",
              type: 1,
              html_title: "Soạn bài Luyện tập lập luận giải thích | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57427",
              title: "Viết bài tập làm văn số 6: Văn lập luận giải thích",
              url: "soan-van-7_viet-bai-tap-lam-van-so-6-van-lap-luan-giai-thich",
              type: 1,
              html_title: "Soạn bài Viết bài tập làm văn số 6: Văn lập luận giải thích | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57426",
          title: "Bài 26",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57425",
              title: "Những trò lố hay là Va-ren và Phan Bội Châu",
              url: "soan-van-7_nhung-tro-lo-hay-la-va-ren-va-phan-boi-chau",
              type: 1,
              html_title: "Soạn bài Những trò lố hay là Va-ren và Phan Bội Châu | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57424",
              title: "Dùng cụm chủ - vị để mở rộng câu: Luyện tập",
              url: "soan-van-7_dung-cum-chu-vi-de-mo-rong-cau-luyen-tap",
              type: 1,
              html_title: "Soạn bài Dùng cụm chủ - vị để mở rộng câu: Luyện tập | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57423",
              title: "Luyện nói: Bài văn giải thích một số vấn đề",
              url: "soan-van-7_luyen-noi-bai-van-giai-thich-mot-van-de",
              type: 1,
              html_title: "Soạn bài Luyện nói: Bài văn giải thích một số vấn đề | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57422",
          title: "Bài 27",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57421",
              title: "Ca Huế trên sông Hương",
              url: "soan-van-7_ca-hue-tren-song-huong",
              type: 1,
              html_title: "Soạn bài Ca Huế trên sông Hương | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57420",
              title: "Liệt kê",
              url: "soan-van-7_liet-ke",
              type: 1,
              html_title: "Soạn bài Liệt kê | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5741f",
              title: "Trả bài tập làm văn số 6",
              url: "soan-van-7_tra-bai-tap-lam-van-so-6",
              type: 1,
              html_title: "Soạn bài Trả bài tập làm văn số 6 | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5741e",
              title: "Tìm hiểu chung về văn bản hành chính",
              url: "soan-van-7_tim-hieu-chung-ve-van-ban-hanh-chinh",
              type: 1,
              html_title: "Soạn bài Tìm hiểu chung về văn bản hành chính | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5741d",
          title: "Bài 28",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5741c",
              title: "Quan Âm Thị Kính",
              url: "soan-van-7_quan-am-thi-kinh",
              type: 1,
              html_title: "Soạn bài Quan Âm Thị Kính | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5741b",
              title: "Dấu chấm lửng và dấu chấm phẩy",
              url: "soan-van-7_dau-cham-lung-va-dau-cham-phay",
              type: 1,
              html_title: "Soạn bài Dấu chấm lửng và dấu chấm phẩy | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5741a",
              title: "Văn bản đề nghị",
              url: "soan-van-7_van-ban-de-nghi",
              type: 1,
              html_title: "Soạn bài Văn bản đề nghị | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57419",
          title: "Bài 29",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57418",
              title: "Ôn tập phần văn",
              url: "soan-van-7_on-tap-phan-van",
              type: 1,
              html_title: "Soạn bài Ôn tập phần văn | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57417",
              title: "Dấu gạch ngang",
              url: "soan-van-7_dau-gach-ngang",
              type: 1,
              html_title: "Soạn bài Dấu gạch ngang | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57416",
              title: "Ôn tập phần Tiếng Việt",
              url: "soan-van-7_on-tap-phan-tieng-viet",
              type: 1,
              html_title: "Soạn bài Ôn tập phần Tiếng Việt (học kì 2) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57415",
              title: "Văn bản báo cáo",
              url: "soan-van-7_van-ban-bao-cao",
              type: 1,
              html_title: "Soạn bài Văn bản báo cáo | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57414",
          title: "Bài 30",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57413",
              title: "Kiểm tra phần Văn",
              url: "soan-van-7_kiem-tra-phan-van",
              type: 1,
              html_title: "Soạn bài Kiểm tra phần Văn | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57412",
              title: "Luyện tập làm văn bản đề nghị và báo cáo",
              url: "soan-van-7_luyen-tap-lam-van-ban-de-nghi-bao-cao",
              type: 1,
              html_title: "Soạn bài Luyện tập làm văn bản đề nghị và báo cáo | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57411",
              title: "Ôn tập phần Tập làm văn",
              url: "soan-van-7_on-tap-phan-tap-lam-van",
              type: 1,
              html_title: "Soạn bài Ôn tập phần Tập làm văn | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57410",
          title: "Bài 31",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5740f",
              title: "Ôn tập phần Tiếng Việt (tiếp theo)",
              url: "soan-van-7_on-tap-phan-tieng-viet-tiep",
              type: 1,
              html_title: "Soạn bài Ôn tập phần Tiếng Việt (tiếp theo - học kì 2) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5740e",
              title: "Kiểm tra tổng hợp cuối năm",
              url: "soan-van-7_kiem-tra-tong-hop-cuoi-nam",
              type: 1,
              html_title: "Soạn bài Kiểm tra tổng hợp cuối năm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5740d",
          title: "Bài 32",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc5740c",
              title: "Chương trình địa phương (Phần Văn và Tập làm văn)",
              url: "soan-van-7_chuong-trinh-dia-phuong",
              type: 1,
              html_title: "Soạn bài Chương trình địa phương Phần Văn và Tập làm văn Tập 2 | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc5740b",
              title: "Hoạt động ngữ văn",
              url: "soan-van-7_hoat-dong-ngu-van",
              type: 1,
              html_title: "Soạn bài Hoạt động ngữ văn | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc5740a",
          title: "Bài 33",
          url: "",
          type: 0,
          html_title: ""
        },
        {
          children: [
            {
              children: [],
              _id: "5de9d6e5843e27002cc57409",
              title: "Chương trình địa phương (phần Tiếng Việt): Rèn luyện chính tả",
              url: "soan-van-7_chuong-trinh-dia-phuong-bai-34",
              type: 1,
              html_title: "Soạn bài Chương trình địa phương (phần Tiếng Việt) Rèn luyện chính tả (học kì 2) | Ngắn nhất Soạn văn 7"
            },
            {
              children: [],
              _id: "5de9d6e5843e27002cc57408",
              title: "Trả bài kiểm tra tổng hợp cuối năm",
              url: "soan-van-7_tra-bai-kiem-tra-tong-hop-cuoi-nam",
              type: 1,
              html_title: "Soạn bài Trả bài kiểm tra tổng hợp cuối năm | Ngắn nhất Soạn văn 7"
            }
          ],
          _id: "5de9d6e5843e27002cc57407",
          title: "Bài 34",
          url: "",
          type: 0,
          html_title: ""
        }
      ],
      _id: "5de9d6e5843e27002cc57406",
      title: "Soạn văn 7 Tập 2",
      url: "soan-van-7_soan-van-7-tap-2",
      type: 0,
      html_title: "Soạn văn lớp 7 Tập 2 ngắn nhất | Soạn bài lớp 7 Tập 2"
    }
  ],
  created_at: "2019-12-06T04:19:49.510Z",
  __v: 0,
  updated_at: "2020-05-20T10:30:09.599Z",
  html_title: "Soạn Văn lớp 7 ngắn nhất | Soạn bài lớp 7 | Để học tốt ngữ văn 7"
}