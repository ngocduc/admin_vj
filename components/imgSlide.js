// import { render } from 'react-dom'
import React, { useRef, useState, useEffect, useCallback } from 'react'
import { useSprings, animated, useTransition, config } from 'react-spring'
import styled from 'styled-components';
import { Icon } from 'antd'
import { debounce, throttle } from 'lodash';
// import './styles.css'

const pages = [
  'https://images.pexels.com/photos/62689/pexels-photo-62689.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  'https://images.pexels.com/photos/296878/pexels-photo-296878.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  'https://images.pexels.com/photos/1509428/pexels-photo-1509428.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  'https://images.pexels.com/photos/351265/pexels-photo-351265.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  'https://images.pexels.com/photos/924675/pexels-photo-924675.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
]

const items = [
  {
    color: 'red',
    opacity: 1,// .5,
    left: 0,
  }, {
    color: 'blue',
    opacity: 1,// 1,
    left: 1,
  }, {
    color: 'blue',
    opacity: 1,// 1,
    left: 2,
  }, {
    color: 'green',
    opacity: 1,// .2,
    left: 3,
  }, {
    color: 'orange',
    opacity: 1,// .8,
    left: 4,
  },
];

const App = () => {
  const itemLength = items.length;
  const [prop, setProp] = useState(items);
  const [index, setIndex] = useState(0);
  const [direction, setDirection] = useState({});

  const handleSwitch = debounce(function () {
    const newProps = prop.map((i, indexPr) => {
      const { left } = i;
      let nextLeft = left - 1;
      if (nextLeft == -1) {
        return { ...i, left: itemLength - 1, opacity: 0 }
      }
      return { ...i, left: nextLeft, opacity: 1 }
    })
    setProp(newProps)

  }, 600, { leading: false, trailing: true });

  const handleSwitchRight = debounce(function () {
    const newProps = prop.map((i) => {
      const { left } = i;
      let nextLeft = left + 1;
      if (nextLeft == itemLength) {
        return { ...i, left: 0, opacity: 0 }
      }
      return { ...i, left: nextLeft, opacity: 1 }
    })
    setProp(newProps)
  }, 600, { leading: false, trailing: true });

  useInterval(() => {
    if (direction.next == 'right') handleSwitchRight(index)
    else handleSwitch(index)
  }
    , 3000, direction)


  const springs = useSprings(itemLength, prop.map(({ color, opacity, left, display = 'block', scl = 1 }, index) => {
    const scale = scl ? 1 : 0.7
    return {
      color,
      // opacity,
      display: opacity === 1 ? 'block' : 'none',
      left: `${(left - 1) * 33.333}%`,
      // transform: `scale(${scale}, ${scale})`,
      config: {
        ...config.default,
        duration: 300,
      }
    }
  }));

  return (
    <Wrapper>
      {springs.map((animation, key) => {
        const { transform } = animation;
        console.log(animation.opacity)
        return <animated.div key={key} style={{
          width: '33.333%',
          top: '5%',
          height: '40vh',
          position: 'absolute',
          // background: animation.color.interpolate(i => i),
          // opacity: animation.opacity,
          display: animation.display,// === 1 ? 'block': 'none',
          left: animation.left,
          // transform,
        }}>
          <div className="side-image" style={{
            backgroundImage: `url(./bg1.jpg)`,
            height: "100%",
            width: "90%",

            marginLeft: '5%'
          }}>
            ----------------{key}

          </div>
        </animated.div>
      })}
      <div
        style={{ zIndex: '10', position: 'absolute', top: '50%' }}
        onClick={() => { handleSwitch(index), setDirection({ next: 'left' }) }}>
        <Icon type="search"></Icon>
      </div>
      <div
        style={{ zIndex: '10', position: 'absolute', top: '50%', right: '0' }}
        onClick={() => {
          handleSwitchRight(index); setDirection({ next: 'right' })
        }}>
        <Icon type="search"></Icon>
      </div>
    </Wrapper>
  )
};
const Wrapper = styled.div`
  position: relative;
  height: 45vh;
  width: 100%;
  background: #defefe;
  overflow: hidden;
`
export default App;
// render(<Viewpager />, document.getElementById('root'))
const useInterval = (cb, time, reset = false) => {
  const callback = useRef();

  useEffect(() => {
    callback.current = cb;
  }, [cb])
  useEffect(() => {
    const tic = () => callback.current()
    const id = setInterval(tic, time);
    return () => {
      clearInterval(id);
    }
  }, [time, reset]);

}
